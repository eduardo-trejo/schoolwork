/**
 * Title: bubble_shooter.c
 * Abstract: This program is a bubble shooter game. The bubbles(ball) will go to random path
 *           depending on random generation. You will have 2 shots to hit the ball before it
 *           arrives to the 5th wall location.
 * Author: Eduardo Trejo
 * ID: 6787
 * Date: 09/28/2014
 */
#include <allegro.h>

#define WHITE makecol(255,255,255)
#define BLACK makecol(0,0,0)
#define BLUE makecol(0,0,255)

BITMAP *buffer;
BITMAP *crosshair;

const int BLOCKNUM = 5;

typedef struct POINT
{
	int x, y;
}POINT;

POINT points[2000];
int curpoint, totalpoints;
int startx, starty, endx, endy;
int destroyed = 1;
int mx, my, mb;
int score = 0;
int balls = 5;
int way = 0;
int shot = 0;
POINT blocks[5];
int i;
int waitcounter = 0;

void updatecounters(){
	textprintf_right_ex(buffer, font, SCREEN_W - 5, 1, WHITE, BLACK, "Score: %d", score);
	textprintf_ex(buffer, font, 5, 1, WHITE, BLACK, "Ball Left: %d", balls);
}

void drawBlocks(){
	rect(buffer, 0, 12, SCREEN_W - 2, SCREEN_H - 2, WHITE);
	if (blocks[0].x == 0)
		return;
	for (i = 0; i < BLOCKNUM; i++){
		rectfill(buffer, blocks[i].x - 25, blocks[i].y - 25, blocks[i].x + 25, blocks[i].y + 25, BLUE);
	}
}

void shuffleBlocks(){
	int rand1, rand2;
	POINT tmp;
	for (i = 0; i < 10; i++){
		rand1 = rand() % BLOCKNUM;
		rand2 = rand() % BLOCKNUM;
		tmp = blocks[rand1];
		blocks[rand1] = blocks[rand2];
		blocks[rand2] = tmp;
	}
}

void end(){
	rectfill(screen, 0, 12, SCREEN_W - 1, SCREEN_H - 1, BLACK);
	textprintf_centre_ex(screen, font, SCREEN_W / 2, SCREEN_H / 2, WHITE, -1, "Game over - Your score: %d", score);
	while (!key[KEY_ESC]) rest(200);
	set_mouse_sprite(NULL);
	destroy_bitmap(crosshair);
	allegro_exit();
	exit(0);
}

void doline(BITMAP *bmp, int x, int y, int d)
{
	points[totalpoints].x = x;
	points[totalpoints].y = y;
	totalpoints++;
}

void startBall(int way){
	destroyed = 0;
	totalpoints = 0;
	curpoint = 0;

	if (way == 0){
		balls--;
		shot = 0;
		shuffleBlocks();
		updatecounters();
		startx = 0;
		starty = (rand() % (SCREEN_H - 23)) + 22;
	}
	else {
		startx = blocks[way - 1].x;
		starty = blocks[way - 1].y;
	}

	if (way == 5){
		endx = SCREEN_W - 1;
		endy = (rand() % (SCREEN_H - 23)) + 22;
	}
	else {
		endx = blocks[way].x;
		endy = blocks[way].y;
	}

	do_line(buffer, startx, starty, endx, endy, BLACK, &doline);
}

void moveBall(){
	int x = points[curpoint].x;
	int y = points[curpoint].y;

	scare_mouse();

	circlefill(buffer, points[curpoint - 1].x, points[curpoint - 1].y, 10, BLACK);

	if (getpixel(screen, mx, my) == WHITE && mb && shot < 3){
		destroyed++;
		score++;
		updatecounters();
	}
	else{
		circlefill(buffer, x, y, 10, WHITE);
	}

	drawBlocks();

	unscare_mouse();

	curpoint++;
	if (curpoint >= totalpoints){
		if (way < BLOCKNUM){
			rest(rand() % 3000);
			startBall(++way);
		}
		else {
			circlefill(buffer, x, y, 10, BLACK);
			destroyed++;
		}
	}
}

int main(void)
{
	allegro_init();
	set_color_depth(16);
	set_gfx_mode(GFX_SAFE, 640, 480, 0, 0);
	install_keyboard();
	install_mouse();
	install_timer();
	srand(time(NULL));

	buffer = create_bitmap(640, 480);

	updatecounters();

	for (i = 0; i < 5; i++){
		blocks[i].x = (rand() % (SCREEN_W - 53)) + 23;
		blocks[i].y = (rand() % (SCREEN_H - 63)) + 33;
	}

	drawBlocks();

	crosshair = load_bitmap("crosshair.bmp", NULL);
	set_mouse_sprite(crosshair);
	set_mouse_sprite_focus(15, 15);

	show_mouse(screen);

	while (!key[KEY_ESC]){
		mx = mouse_x;
		my = mouse_y;
		mb = (mouse_b & 1);
		if (mb && waitcounter <= 0){
			shot++;
			waitcounter = 50;
			updatecounters();
		}
		else if (waitcounter > 0){
			waitcounter--;
		}

		if (destroyed){
			if (balls > 0){
				way = 0;
				startBall(way);
			}
			else
				end();
		}

		moveBall();

		blit(buffer, screen, 0, 0, 0, 0, 640, 480);

		rest(10);
	}

	set_mouse_sprite(NULL);
	destroy_bitmap(crosshair);
	allegro_exit();

	return 0;
}
END_OF_MAIN();